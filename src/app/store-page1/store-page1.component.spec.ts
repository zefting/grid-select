import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePage1Component } from './store-page1.component';

describe('StorePage1Component', () => {
  let component: StorePage1Component;
  let fixture: ComponentFixture<StorePage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
