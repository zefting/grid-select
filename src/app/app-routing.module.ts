import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { StorePage1Component } from './store-page1/store-page1.component';
import { StorePage2Component } from './store-page2/store-page2.component';
import { StorePage3Component } from './store-page3/store-page3.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'page1', component: StorePage1Component },
  { path: 'page2', component: StorePage2Component },
  { path: 'page3', component: StorePage3Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
