import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePage2Component } from './store-page2.component';

describe('StorePage2Component', () => {
  let component: StorePage2Component;
  let fixture: ComponentFixture<StorePage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
