import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePage3Component } from './store-page3.component';

describe('StorePage3Component', () => {
  let component: StorePage3Component;
  let fixture: ComponentFixture<StorePage3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePage3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePage3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
