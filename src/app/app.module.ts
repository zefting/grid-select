import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { StorePage1Component } from './store-page1/store-page1.component';
import { StorePage2Component } from './store-page2/store-page2.component';
import { StorePage3Component } from './store-page3/store-page3.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StorePage1Component,
    StorePage2Component,
    StorePage3Component,
  ],
  imports: [BrowserModule, AppRoutingModule, NgbModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
